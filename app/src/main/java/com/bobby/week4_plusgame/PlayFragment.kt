package com.bobby.week4_plusgame

import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.bobby.week4_plusgame.databinding.FragmentPlayBinding
import timber.log.Timber

class PlayFragment : Fragment() {

    private lateinit var viewModel: GameViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentPlayBinding>(
            inflater,
            R.layout.fragment_play, container, false
        )
        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)
        resources
        binding.gameViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        val typeGame = MenuFragmentArgs.fromBundle(requireArguments()).typeGame

        binding.btnBackMenu.setOnClickListener { view ->
            view.findNavController()
                .navigate(
                    PlayFragmentDirections.actionPlayFragmentToMenuFragment(
                        typeGame = typeGame,
                        correct = viewModel.correct.value!!,
                        incorrect = viewModel.incorrect.value!!
                    )
                )
        }

        Timber.i("Called ViewModelProvider.get")
        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)
        viewModel.play(typeGame)
        return binding.root
    }

}