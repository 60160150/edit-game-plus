package com.bobby.week4_plusgame

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
class ScoreViewModelFactory(private val finalCorrect: Int, private val finalIncorrect: Int) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ScoreViewModel::class.java)) {
            return ScoreViewModel(finalCorrect, finalIncorrect) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}