package com.bobby.week4_plusgame

import android.content.res.Resources
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.bobby.week4_plusgame.databinding.FragmentMenuBinding


const val KEY_MATH_CORRECT = "scoreMathPlusCorrect"
const val KEY_MATH_INCORRECT = "scoreMathPlusIncorrect"

class MenuFragment : Fragment() {

    private lateinit var viewModel: ScoreViewModel
    private lateinit var viewModelFactory: ScoreViewModelFactory

    var typeGame = ""

    private var scoreMathCorrect = 0
    private var scoreMathIncorrect = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        if(savedInstanceState != null) {
            scoreMathCorrect = savedInstanceState.getInt(KEY_MATH_CORRECT, 0)
            scoreMathIncorrect = savedInstanceState.getInt(KEY_MATH_INCORRECT, 0)
        }
        val binding = DataBindingUtil.inflate<FragmentMenuBinding>(inflater,
            R.layout.fragment_menu,container,false)
        viewModelFactory = ScoreViewModelFactory(
            MenuFragmentArgs.fromBundle(requireArguments()).correct,
            MenuFragmentArgs.fromBundle(requireArguments()).incorrect)
        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(ScoreViewModel::class.java)
        binding.scoreViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        scoreMathCorrect = viewModel.correct.value!!
        scoreMathIncorrect = viewModel.incorrect.value!!
        binding.apply {
            btnGamePlus.setOnClickListener { view ->
                view.findNavController()
                    .navigate(MenuFragmentDirections.actionMenuFragmentToPlayFragment(
                        typeGame ="+"
                    ))
            }
            btnGameMinus.setOnClickListener { view ->
                view.findNavController()
                    .navigate(MenuFragmentDirections.actionMenuFragmentToPlayFragment(
                        typeGame ="-"
                    ))
            }
            btnGameMultiply.setOnClickListener { view ->
                view.findNavController()
                    .navigate(MenuFragmentDirections.actionMenuFragmentToPlayFragment(
                        typeGame ="x"
                    ))
            }
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.option_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return view?.findNavController()?.let {
            NavigationUI.onNavDestinationSelected(
                item,
                it
            )
        }!! || super.onOptionsItemSelected(item)
    }
    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(KEY_MATH_CORRECT, scoreMathCorrect)
        outState.putInt(KEY_MATH_INCORRECT, scoreMathIncorrect)
        super.onSaveInstanceState(outState)
    }
}